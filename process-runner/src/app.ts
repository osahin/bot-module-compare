import express, { Request, Response } from 'express';
import { logger } from './services/logger-service';
import { json } from 'body-parser';
import cors from 'cors';
import { MinioService } from './services/minio-service';
import { ProcessService } from './services/process-service';

if (
  !process.env.ENDPOINT ||
  !process.env.ACCESS_KEY ||
  !process.env.SECRET_KEY ||
  !process.env.MINIO_PORT ||
  !process.env.BUCKET_NAME ||
  !process.env.FILE_PATH
) {
  throw new Error(
    'Make sure ENDPOINT, ACCESS_KEY, SECRET_KEY, MINIO_PORT, BUCKET_NAME and FILE_PATH environment variables are defined.'
  );
}

const minioService = new MinioService(
  {
    endPoint: process.env.ENDPOINT,
    port: Number(process.env.MINIO_PORT),
    useSSL: false,
    accessKey: process.env.ACCESS_KEY,
    secretKey: process.env.SECRET_KEY,
  },
  process.env.BUCKET_NAME,
  process.env.FILE_PATH
);

minioService.on('completed', ({ filePath, fileName }) => {
  ProcessService.createProcess(filePath, fileName);
});

const app = express();
app.use(
  cors({
    origin: true,
    credentials: true,
  })
);
app.use(json());

app.get('/start-bot/:id', (req: Request, res: Response) => {
  logger.info(`Inbound request: `, { req, res });
  const fileName = req.params.id;
  minioService.download(fileName);
  res.send({ result: 'Your request has been added to the queue.' });
});

app.get('/stop-bot/:id', (req: Request, res: Response) => {
  logger.info(`Inbound request: `, { req, res });
  const fileName = req.params.id;
  res.send({ result: ProcessService.endProcess(fileName) });
});

app.get('/is-running/:id', (req: Request, res: Response) => {
  logger.info(`Inbound request: `, { req, res });
  const fileName = req.params.id;
  res.send({ result: ProcessService.isRunning(fileName) });
});
app.post('/states-of-processes', (req: Request, res: Response) => {
  logger.info(`Inbound request: `, { req, res });
  if (!req.body || !Array.isArray(req.body)) {
    return res.status(400).json({ error: 'Invalid paramaters' });
  }
  const processMap = ProcessService.stateOfProcesses(req.body);
  res.send(processMap);
});
app.get('/num-of-running-bots', (req: Request, res: Response) => {
  logger.info(`Inbound request: `, { req, res });
  res.send({ result: ProcessService.numOfRunningProcesses() });
});

process.once('SIGINT', () => ProcessService.stopAllBots());
process.once('SIGTERM', () => ProcessService.stopAllBots());

export { app };
