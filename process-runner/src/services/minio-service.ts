import { Client, ClientOptions } from 'minio';
import fs from 'fs';
import { EventEmitter } from 'stream';
import { logger } from './logger-service';

export class MinioService extends EventEmitter {
  readonly folderPath: string;
  minioClient: Client;
  bucketName: string;

  constructor(options: ClientOptions, bucketName: string, folderPath: string) {
    super();
    this.bucketName = bucketName;
    this.folderPath = folderPath;
    this.minioClient = new Client(options);
  }

  download(fileName: string) {
    const filePath = this.folderPath + fileName;
    this.minioClient.bucketExists(this.bucketName, (error) => {
      if (error) {
        logger.error(error);
        this.emit('error', { fileName, error });
      } else {
        let data: any;
        this.minioClient.getObject(this.bucketName, fileName, (err, objStream) => {
          if (err) {
            logger.error(err);
            this.emit('error', { fileName, err });
            return;
          }
          objStream.on('data', (chunk) => {
            if (data) {
              data = Buffer.concat([data, chunk]);
            } else {
              data = Buffer.alloc(chunk.length, chunk, 'utf8');
            }
          });
          objStream.on('end', () => {
            try {
              if (!fs.existsSync(this.folderPath)) {
                logger.info(`creating ${this.folderPath} dir`);
                fs.mkdirSync(this.folderPath);
              }
              fs.writeFileSync(filePath, data);
            } catch (error) {
              logger.error(error);
              this.emit('error', error);
            }
            logger.info('Filename:' + filePath + ' has been downloaded!');
            this.emit('completed', { filePath, fileName });
          });
          objStream.on('error', (err) => {
            logger.error(err);
            this.emit('error', { fileName, err });
          });
        });
      }
    });
  }
}
